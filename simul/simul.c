#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

/*
 * This file models behaviour of the firmware used to detect the DCF77 signal.
 */
#define alfa_float 0.1
#define i_alfa ((int32_t) ((1<<15) * alfa_float))
#define i_1_alfa ((int32_t) ((1<<15) * (1-alfa_float)))

//Generation of the simplified DCF77 signal (only AM, no phase modulation, amplitude changed stepwise)
double stime = 0.0; //Simulation time
//Sampling period for quadrature sampling
double smp_per = 157.0/48e6;
uint16_t ADC_Buf[4];
double fdcf77 = 77.5e3; //Frequency of the DCF77 signal
double fm77 = 1e3; //Frequency of the AM modulation

//Function defining the value of the signal as a function of time
uint16_t val_smp(void)
{
  //Find the amplitude
  double ampl=1000.0;
  double val;
  if(cos(stime*2*M_PI*fm77)<0.0) {
    ampl /= 2.0; 
  }
  val = 2048.0+ampl*cos(stime*2*M_PI*fdcf77);
  return (uint16_t) val;
}

static inline int16_t lp_filter1(int16_t old, uint16_t v1, uint16_t v2)
{
  int32_t tmp;
  tmp = old * (int32_t) i_1_alfa;
  tmp = tmp + (int32_t) i_alfa * ((int32_t) v1 - (int32_t) v2);
  return (int16_t) (tmp >> 15);
}

int main(int argc, char * argv[]) {
  int16_t c = 0;
  int step=0;
  int fout;
  int16_t I=0,Q = 0;
  fout=open("dump.bin",O_CREAT | O_WRONLY | O_TRUNC);
  if(fout<0) {
    perror("I can't open the output file\n");
    exit(1);
  }
  while(stime < 10.0) {
    //Take 4 samples to calculate I and Q
    int j;
    for (j=0;j<4;j++) {
      ADC_Buf[j]=val_smp();
      stime += smp_per;
    }
    //When 4 samples are taken, update the I and Q signals as in firmware
    I = lp_filter1(I, ADC_Buf[0] & 0xfff, ADC_Buf[2] & 0xfff);
    Q = lp_filter1(Q, ADC_Buf[1] & 0xfff, ADC_Buf[3] & 0xfff);
    //After each 7 steps we write the I and Q components, as the firmware does
    step += 1;
    printf("%g\n",stime);
    if (step == 7) {
      int res;
      int16_t dout[2];
      printf("written\n");
      step = 0;
      dout[0] = I & 0xfffe;
      dout[1] = Q | 0x0001;
      res=write(fout,dout,sizeof(dout));
      if(res<0) {
	perror("I can't write data to the the output file\n");
	exit(1);
      }
    }
  }
  close(fout);
  return 0;
}
