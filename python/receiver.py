#!/usr/bin/python
# -*- coding: utf-8 -*-
import termios
#import TERMIOS
import os
import time
import math

tty_fname="/dev/ttyACM0"

def tty_init(fd):
    told = termios.tcgetattr(fd)
    tnew = termios.tcgetattr(fd)
    tnew[0] = 0 #iflag
    tnew[1] = 0 #oflag
    tnew[2] = termios.CS8 | termios.CSTOPB | termios.CLOCAL | termios.CREAD # cflag
    tnew[3] = 0 #lflag
    tnew[4] = termios.B115200 # ispeed
    tnew[5] = termios.B115200 # ospeed
    tnew[6] = 32*[chr(0)]
    tnew[6][termios.VMIN] = 0 #cc
    tnew[6][termios.VTIME] = 1 #cc
    try:
        termios.tcflush(fd,termios.TCIFLUSH)
        termios.tcsetattr(fd, termios.TCSANOW, tnew)
    except:
        termios.tcsetattr(fd, termios.TCSADRAIN, told)

def read_char(fd,tout):
    znak=None
    start = time.time()
    while ( time.time() < start + tout ):
        try:
            znak=os.read(fd,1)
            break
        except:
            pass #print "."
    return znak

#tty_fd=os.open('/dev/ttyS0',os.O_RDWR | os.O_NOCTTY | os.O_NONBLOCK)
#os.system("setserial "+tty_fname+" baud_base 3000000 spd_cust divisor 768")
tty_fd=os.open(tty_fname,os.O_RDWR | os.O_NOCTTY )
midi_out=open("./dump.bin","w")
tty_init(tty_fd)
os.write(tty_fd,"624\n")
while True:
    znak=read_char(tty_fd,1)
    if znak:
      #if znak==chr(0x40):
      #        znak=chr(0xc0)
      midi_out.write(znak)
      midi_out.flush()
      #print hex(ord(znak))

